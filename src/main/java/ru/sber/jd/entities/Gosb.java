package ru.sber.jd.entities;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class Gosb {

    Integer gosbId;
    String gosbName;
    String tbName;
    String segment;
    Double creditDueValue;
    Double creditOverdueValue;


}
