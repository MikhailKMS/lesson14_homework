package ru.sber.jd;

import ru.sber.jd.entities.Gosb;
import ru.sber.jd.utils.DbUtils;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;


public class Main {


    public static void main(String[] args) throws SQLException, ParseException {

        Connection connection = DbUtils.createConnetion();

        DbUtils.dropTable(connection);
        DbUtils.createTable(connection);



        DbUtils.insertGosb(connection, Gosb.builder()
                .gosbId(8610)
                .gosbName("Банк Татарстан")
                .tbName("ВВБ")
                .segment("ММБ")
                .creditDueValue(8000d)
                .creditOverdueValue(10d)
                .build());
        DbUtils.insertGosb(connection, Gosb.builder()
                .gosbId(8611)
                .gosbName("Владимирское ГОСБ")
                .tbName("ВВБ")
                .segment("КСБ")
                .creditDueValue(14000d)
                .creditOverdueValue(9d)
                .build());
        DbUtils.insertGosb(connection, Gosb.builder()
                .gosbId(8612)
                .gosbName("Кировское ГОСБ")
                .tbName("ВВБ")
                .segment("РГС")
                .creditDueValue(50d)
                .creditOverdueValue(1d)
                .build());

        System.out.println("Реализация select *");
        for (Gosb gosb : DbUtils.selectAll(connection)) {
            System.out.println(gosb);
        }

        System.out.println("\nРеализация where");
        for (Gosb gosb : DbUtils.selectSegment(connection)) {
            System.out.println(gosb);
        }

        System.out.println("\nРеализация group by");
        for (Gosb gosb : DbUtils.selectDue(connection)) {
            System.out.println(gosb);
        }

        connection.close();

    }


}
