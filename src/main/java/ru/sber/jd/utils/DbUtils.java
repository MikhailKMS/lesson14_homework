package ru.sber.jd.utils;

import ru.sber.jd.entities.Gosb;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbUtils {

    public static Connection createConnetion() {
        try {
            return DriverManager.getConnection("jdbc:h2:~/sql;MODE=PostgreSQL", "sa", "");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }


    public static void dropTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("drop table if exists gosb");
        connection.commit();
    }

    public static void createTable(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("create table if not exists gosb (gosb_id integer, gosb_name text, tb_name text, segment text, due double, overdue double)");
        connection.commit();
    }

    public static void deleteALl(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("delete from gosb");
        connection.commit();
    }

    public static void insertGosb(Connection connection, Gosb gosb) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute(String.format("insert into gosb (gosb_id, gosb_name, tb_name, segment, due, overdue)" +
                "values (%s, '%s', '%s', '%s', %s, %s)", gosb.getGosbId(), gosb.getGosbName(), gosb.getTbName(), gosb.getSegment(), gosb.getCreditDueValue(), gosb.getCreditOverdueValue()));
        connection.commit();
    }



    public static List<Gosb> selectAll(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from gosb");

        List<Gosb> gosbs = new ArrayList<>();

        while (resultSet.next()) {
            Integer gosbID = resultSet.getInt(1);
            String gosbName = resultSet.getString(2);
            String tbName = resultSet.getString(3);
            String segment = resultSet.getString(4);
            Double creditDueValue = resultSet.getDouble(5);
            Double creditOverdueValue = resultSet.getDouble(6);

            gosbs.add(Gosb.builder()
                    .gosbId(gosbID)
                    .gosbName(gosbName)
                    .tbName(tbName)
                    .segment(segment)
                    .creditDueValue(creditDueValue)
                    .creditOverdueValue(creditOverdueValue)
                    .build()
            );
        }
        return gosbs;
    }

    public static List<Gosb> selectSegment(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select * from gosb where segment = 'ММБ'");

        List<Gosb> gosbs = new ArrayList<>();

        while (resultSet.next()) {
            Integer gosbID = resultSet.getInt(1);
            String gosbName = resultSet.getString(2);
            String tbName = resultSet.getString(3);
            String segment = resultSet.getString(4);
            Double creditDueValue = resultSet.getDouble(5);
            Double creditOverdueValue = resultSet.getDouble(6);

            gosbs.add(Gosb.builder()
                    .gosbId(gosbID)
                    .gosbName(gosbName)
                    .tbName(tbName)
                    .segment(segment)
                    .creditDueValue(creditDueValue)
                    .creditOverdueValue(creditOverdueValue)
                    .build()
            );
        }
        return gosbs;
    }

    public static List<Gosb> selectDue(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("select tb_name, sum(due) from gosb group by tb_name");

        List<Gosb> gosbs = new ArrayList<>();

        while (resultSet.next()) {

            String tbName = resultSet.getString(1);
            Double creditDueValue = resultSet.getDouble(2);

            gosbs.add(Gosb.builder()
                    .tbName(tbName)
                    .creditDueValue(creditDueValue)
                    .build()
            );
        }
        return gosbs;
    }




}
